package com.kars.listener.ui

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.kars.listener.LogTag
import com.kars.listener.R
import com.kars.listener.service.ParserService

class MainActivity : AppCompatActivity(), LogTag {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (!ParserService.isRunning)
            startService(Intent(this, ParserService::class.java))
    }

}
