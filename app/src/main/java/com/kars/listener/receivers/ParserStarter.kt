package com.kars.listener.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.kars.listener.LogTag
import com.kars.listener.service.ParserService

class ParserStarter : BroadcastReceiver(), LogTag {

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action == Intent.ACTION_BOOT_COMPLETED && context != null) {
            val service = context.startService(Intent(context, ParserService::class.java))
            Log.d(LOGTAG, "trying to start service")
            if (service == null) {
                Log.e(LOGTAG, "Can't start service ${ParserService::javaClass.name}")
            }
        } else {
            Log.e(LOGTAG, "context is null || Received unexpected intent $intent")
        }
    }
}