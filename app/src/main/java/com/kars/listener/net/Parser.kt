package com.kars.listener.net

import android.util.Log
import com.kars.listener.LogTag
import com.kars.listener.db.Post
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element
import java.util.Calendar


class Parser {

    companion object : LogTag {

        fun getPosts(): HashSet<Post> {
            val posts = HashSet<Post>()
            val doc = Jsoup.connect(URL)
                .userAgent(AGENT_HEADER)
                .header("Accept-Language", LANGUAGE_HEADER)
                .header("Accept-Encoding", ENCODING_HEADER)
                .header("Accept", ACCEPT_HEADER)
                .timeout(TIMEOUT)
                .get()
            getAllTitles(doc).forEach { title ->
                val tsString = getTimestamp(title)
                var ts = -1L
                if (tsString != null)
                    ts = toUnix(tsString)
                posts.add(Post(ts, title.text(), fullLink(title)))
            }
            return posts
        }

        private fun getAllTitles(doc: Document): List<Element> {
            return doc.select("a").filter {
                it.attr("data-click-id").equals("body", true)
            }
        }

        private fun toUnix(tsFull: String): Long {
            val cal = Calendar.getInstance()
            val dur = -getDuration(tsFull)
            when {
                tsFull.contains("minute") -> cal.add(Calendar.MINUTE, dur)
                tsFull.contains("hour") -> cal.add(Calendar.HOUR, dur)
                tsFull.contains("day") -> cal.add(Calendar.DAY_OF_MONTH, dur)
                tsFull.contains("month") -> cal.add(Calendar.MONTH, dur)
                tsFull.contains("year") -> cal.add(Calendar.YEAR, dur)
            }

            return cal.time.time
        }

        private fun getDuration(tsFull: String): Int {
            return tsFull.substring(0, tsFull.indexOf(' ')).toInt()
        }

        private fun getTimestamp(titleLink: Element): String? {
            val ts = getIDTimestamp(titleLink) ?: getTimestampWithoutID(titleLink)
            if (ts == null)
                Log.w(LOGTAG, "missed both timestamps with and without id: ${titleLink.attr("href")}")
            return ts
        }

        private fun getIDTimestamp(titleLink: Element): String? {
            val timeLinks = titleLink
                .parent()
                .parent()
                .previousElementSibling()
                .select("a")
                .filter {link ->
                    link.attr("data-click-id").equals("timestamp", true)
                }
            if (timeLinks.size == 1) {
//                Log.d(LOGTAG, "timestamp with id: ${titleLink.attr("href")}")
                return timeLinks[0].text()
            }
            return null
        }

        private fun getTimestampWithoutID(titleLink: Element): String? {
            val userLink = titleLink
                .parent()
                .parent()
                .previousElementSibling()
                .select("a")
            if (userLink.size < 1)
                return null
            val next = userLink[0].nextElementSibling()
            if (isTimestampWithoutID(next)) {
//                Log.d(LOGTAG, "timestamp without id: ${titleLink.attr("href")}")
                return next.text()
            }
            return null
        }

        private fun fullLink(title: Element): String {
            return DOMAIN + title.attr("href")
        }

        private fun isTimestampWithoutID(element: Element): Boolean {
            return element.tagName() == "span"
        }

        private const val FRESHNESS = 10

        private const val AGENT_HEADER = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 " +
                "(KHTML, like Gecko) Chrome/72.0.3626.119 Safari/537.36"

        private const val DOMAIN = "http://www.reddit.com"

        private const val URL = "$DOMAIN/r/KindVoice/"

        private const val LANGUAGE_HEADER = "en-US;q=0.8,en;q=0.7"

        private const val ENCODING_HEADER = "gzip, deflate"

        private const val ACCEPT_HEADER = "*/*"

        private const val TIMEOUT = 60 * 1000

    }

}