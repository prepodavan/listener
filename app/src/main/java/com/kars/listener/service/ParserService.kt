package com.kars.listener.service

import android.app.Service
import android.content.Intent
import android.content.SharedPreferences
import android.os.IBinder
import android.preference.PreferenceManager
import android.util.Log
import com.kars.listener.LogTag
import com.kars.listener.db.DBAdapter
import com.kars.listener.db.Post
import com.kars.listener.net.Parser
import com.kars.listener.notification.LocalNotificationManager

class ParserService : Service(), LogTag {

    private lateinit var task: MonitorTask
    private lateinit var prefs: SharedPreferences
    private lateinit var db: DBAdapter

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onDestroy() {
        super.onDestroy()
        task.cancel()
        db.close()
        isRunning = false
        staticPrefs = null
        destroyedLog()
    }

    override fun onCreate() {
        super.onCreate()
        prefs = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        staticPrefs = prefs
        isRunning = true
        db = DBAdapter(applicationContext)
        task = MonitorTask()
        task.start()
        startLog()

    }

    private inner class MonitorTask internal constructor() : Thread("Parser") {

        private var keepRunning = false

        fun cancel() {
            keepRunning = false
        }

        override fun run() {
            Log.d(LOGTAG, "thread is running")
            keepRunning = true
            while (keepRunning) {
                try {
                    doTask()
                    sleep(prefs.getInt(KEY_INTERVAL, DEF_INTERVAL).toLong())
                } catch (e: Exception) {
                    Log.e(LOGTAG, "some exception while doing task or sleep", e)
                }
            }
        }

        private fun doTask() {
            Parser.getPosts()
                .filter(::filterPost)
                .forEach {
                    Log.d(LOGTAG, "new post: $it")
                    save(it)
                    notifyUser(it)
                }
        }

        private fun save(p: Post) {
            db.insert(p)
        }

        private fun filterPost(p: Post): Boolean {
            return isNotExists(p)
        }

        private fun isNotExists(p: Post): Boolean {
            return db.queryPost(p.link) == null
        }

        private fun notifyUser(post: Post) {
            LocalNotificationManager.getInstance(applicationContext).newPost(post, applicationContext)
        }

    }

    companion object {

        private const val runningString = "isPSRunning"

        private const val KEY_INTERVAL = "parse.interval"

        private const val DEF_INTERVAL = 30 * 1000

        private var staticPrefs: SharedPreferences? = null

        var isRunning: Boolean
            get() = staticPrefs?.getBoolean(runningString, false) ?: false
            private set(v) {
                val editor = staticPrefs?.edit()
                editor?.putBoolean(runningString, v)
                editor?.apply()
            }

    }

}