package com.kars.listener.notification

import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.support.v4.app.NotificationCompat
import com.kars.listener.LogTag
import com.kars.listener.R
import com.kars.listener.db.Post
import com.kars.listener.ui.MainActivity


class LocalNotificationManager private constructor(private val nm: NotificationManager) : LogTag {

    fun newPost(p: Post, context: Context) {
        val intent = Intent(context, MainActivity::class.java)
        val pIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)
        nm.notify(TAG, ID, NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(p.title)
            .setContentIntent(pIntent)
            .setAutoCancel(true)
            .build())
    }

    companion object {

        private const val ID = 5077
        private const val CHANNEL_ID = "ytd"
        private const val TAG = "parser"

        fun getInstance(context: Context): LocalNotificationManager {
            val nm = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            return LocalNotificationManager(nm)
        }
    }

}
