package com.kars.listener.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.DataSetObservable
import android.database.DataSetObserver
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import android.util.Log
import com.kars.listener.LogTag


/**
 * DBAdapter class used to manipulate clipboard database.
 * <p>
 * The database contains one table:
 * <pre>
 * Posts TABLE:
 * +----+-------+-------------+------+
 * | ID | TITLE | POSTED_DATE | LINK |
 * +----+-------+-------------+------+
 * </pre>
 */
class DBAdapter(context: Context) : LogTag {

    private inner class DatabaseHelper(context: Context) : SQLiteOpenHelper(context,
        DATABASE_NAME, null, DATABASE_VERSION) {

        override fun onCreate(db: SQLiteDatabase?) {
            db ?: return
            db.execSQL("CREATE TABLE IF NOT EXISTS $POSTS_TABLE_NAME (" +
                    "${BaseColumns._ID} INTEGER PRIMARY KEY," +
                    "${Post.COL_TITLE} TEXT," +
                    "${Post.COL_POSTED_DATE} INTEGER," +
                    "${Post.COL_LINK} TEXT)")
            Log.d(LOGTAG, "created sqlite database table: $POSTS_TABLE_NAME")
        }

        override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
            db ?: return
            Log.w(LOGTAG, "Upgrading database from version $oldVersion to " +
                    "$newVersion, which will destroy all old data")
            drop(db, POSTS_TABLE_NAME)
            onCreate(db)
        }

        fun drop(db: SQLiteDatabase, tableName: String) {
            db.execSQL("DROP TABLE IF EXISTS $tableName")
        }
    }

    private val db = DatabaseHelper(context)

    fun insert(p: Post): Long {
        return write("inserting ${p.link} failed") {
            db.writableDatabase.insert(POSTS_TABLE_NAME,  null, values(p))
        }
    }

    fun update(p: Post): Long {
        return write("updating ${p.link} failed") {
            db.writableDatabase.update(POSTS_TABLE_NAME, values(p), linkClause(p), null).toLong()
        }
    }

    fun delete(p: Post): Long {
        return write("deleting ${p.link} failed") {
            db.writableDatabase.delete(POSTS_TABLE_NAME, linkClause(p), null).toLong()
        }
    }

    fun queryPosts(columns: Array<String>? = POSTS_PROJECTION,
                   selection: String? = null,
                   selectionArgs: Array<String>? = null,
                   orderBy: String? = Post.DEFAULT_SORT_ORDER): Cursor {
        return db.readableDatabase.query(POSTS_TABLE_NAME, columns,
            selection, selectionArgs, null, null, orderBy)
    }

    fun queryPost(id: Int): Post? = build(queryPosts(selection = "${BaseColumns._ID}=$id"))

    fun queryPost(link: String): Post? = build(queryPosts(selection = linkClause(link)))

    fun queryAllPosts(): List<Post> = buildAll(queryPosts())

    fun close() = db.close()

    fun register(observer: DataSetObserver) = mDataSetObservable.registerObserver(observer)

    fun unregister(observer: DataSetObserver) = mDataSetObservable.unregisterObserver(observer)

    private fun write(msg: String, operation: () -> Long): Long {
        val rowId = operation()

        if (rowId < 0) {
            Log.e(LOGTAG, msg)
            return -1
        }

        mDataSetObservable.notifyChanged()
        return rowId
    }

    private fun values(p: Post): ContentValues {
        return ContentValues().apply {
            put(Post.COL_TITLE, p.title)
            put(Post.COL_POSTED_DATE, p.postedDate)
            put(Post.COL_LINK, p.link)
        }
    }

    private fun linkClause(p: Post): String = "${Post.COL_LINK}='${p.link}'"

    private fun linkClause(link: String): String = "${Post.COL_LINK}='$link'"

    companion object {
        private const val DATABASE_NAME = "posts.db"
        private const val DATABASE_VERSION = 2
        private const val POSTS_TABLE_NAME = "posts"
        private val POSTS_PROJECTION = arrayOf(BaseColumns._ID, Post.COL_TITLE, Post.COL_POSTED_DATE, Post.COL_LINK)
        private val mDataSetObservable = DataSetObservable()

        fun build(cursor: Cursor): Post? {
            if (cursor.count < 1)
                return null
            cursor.moveToFirst()
            with(cursor) {
                val id = getLong(getColumnIndex(BaseColumns._ID))
                val title = getString(getColumnIndex(Post.COL_TITLE))
                val link = getString(getColumnIndex(Post.COL_LINK))
                val posted = getLong(getColumnIndex(Post.COL_POSTED_DATE))
                return Post(posted, title, link, id)
            }
        }

        fun buildAll(cursor: Cursor): List<Post> {
            val posts = mutableListOf<Post>()
            with(cursor) {
                while (moveToNext()) {
                    val id = getLong(getColumnIndex(BaseColumns._ID))
                    val title = getString(getColumnIndex(Post.COL_TITLE))
                    val link = getString(getColumnIndex(Post.COL_LINK))
                    val posted = getLong(getColumnIndex(Post.COL_POSTED_DATE))
                    posts.add(Post(posted, title, link, id))
                }
            }
            return posts
        }

    }
}
