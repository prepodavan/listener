package com.kars.listener.db

import android.provider.BaseColumns

data class Post(val postedDate: Long, val title: String, val link: String, var id: Long? = null) {

    companion object : BaseColumns {

        const val COL_TITLE = "title"

        const val COL_POSTED_DATE = "posted_date"

        const val COL_LINK = "link"

        const val DEFAULT_SORT_ORDER = "${BaseColumns._ID} DESC"

    }

}